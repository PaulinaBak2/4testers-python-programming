movies = ['Val', 'Aftersun', 'Stalker', 'Silent_twins', 'Men']
last_movie = movies[-1]
movies.append('Titanic')
movies.append('LOTR')
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'Top Gun 2')
print(movies)

list = ['a@example.com', 'b@example.com']
print(len(list))
print(list[0])
print(list[-1])
list.append('cde@example.com')
print(list)

friend = {
    "name": "Małgorzata",
    "age": 29,
    "hobby": ["fitness", "cooking"]
}
friend_hobbies = friend["hobby"]
print("Hobbies of my friend;", friend_hobbies)
print(f"My friend has {len(friend_hobbies)} hobbies")
friend["hobby"].append('football')
print(friend)
friend["married"] = True
print(friend)


