def display_speed_information(speed):
    if speed > 50:
        print('Slow down! ;(')
    else:
        print('Thank you, your speed is below limit! :)')


def check_normal_conditions(temperature, pressure):
    if temperature == 0 and pressure == 1013:
        return True
    else:
        return False


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    if speed > 100:
        print('You just lost your driving licence :(')
    elif speed > 50:
        print(f'You just got a fine! Fine amount {calculate_fine_amount(speed)} ;(')
    else:
        print("Thank you, your speed is fine")


def arguments(note):
    if not (isinstance(note, float) or isinstance(note, int)):
        return 'N/A'
    if note < 2 or note > 5:
        return 'N/A'
    if 4.5 <= note <= 5.0:
        return "bardzo dobry"
    elif note >= 4.0:
        return "dobry"
    elif note >= 3.0:
        return "dostateczny"
    else:
        return "niedostateczny"


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False


if __name__ == '__main__':
    display_speed_information(50)
    display_speed_information(49)
    display_speed_information(51)

    # Zadanie 2 temperatura i ciśnienie
    print(check_normal_conditions(0, 1013))
    print(check_normal_conditions(1, 1013))
    print(check_normal_conditions(0, 1014))
# Zadanie prędkość
print_the_value_of_speeding_fine_in_built_up_area(101)
print_the_value_of_speeding_fine_in_built_up_area(100)
print_the_value_of_speeding_fine_in_built_up_area(49)
print_the_value_of_speeding_fine_in_built_up_area(50)
print_the_value_of_speeding_fine_in_built_up_area(51)

# Zadanie z ocenami
print(arguments(6))
print(arguments(1))
print(arguments(4))
print(arguments(3))
print(arguments(2))
