import random

# Listy zawierające dane do losowania
first_name = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka', 'James', 'Bob', 'Jan', 'Hans', 'Orestes',
              'Saturnin']
last_name = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
country = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

random_first_name = random.choice(first_name)
print(random_first_name)

random_last_name = random.choice(last_name)
print(random_last_name)

random_country = random.choice(country)
print(random_country)


def generate_random_email():
    domain = 'example.com'
    return f'{random_first_name}.{random_last_name}@{domain}'


print(generate_random_email())

random_age = random.randint(1, 65)
print(random_age)
