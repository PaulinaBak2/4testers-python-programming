# Zadanie 1
def calculate_2nd_power_of_number(input_number):
    return input_number * input_number


def change_Celsius_on_Fahrenheit(temperature_in_celsious):
    return temperature_in_celsious * 1.8 + 32


def volume_of_a_cuboid(a, b, c):
    z = a * b * c
    print(z)
    return z


if __name__ == '__main__':
    # Zadanie 1
    print(calculate_2nd_power_of_number(0))
    print(calculate_2nd_power_of_number(16))
    print(calculate_2nd_power_of_number(2.55))

    # Zadanie 2
    print(change_Celsius_on_Fahrenheit(20))

    # Zadanie 3
    print(volume_of_a_cuboid(3, 5, 7))
