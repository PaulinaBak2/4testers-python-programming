import requests


class PokeAPIHandler:
    base_url = "https://pokepi.co/api/v2"
    pokemon_endpoint = "/pokemon"

    def get_list_of_pokemons(self, params=None):
        response = requests.get(self.base_url + self.pokemon_endpoint, params=params)
        assert response. status_code == 200
        return response
