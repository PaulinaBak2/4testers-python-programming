import requests


class GoRESTHandler:
    base_url = "https://gorest.co.in/public/v2/"
    users_endpoint = "/users"

    headers = {
        "Autorization": "Bearer 610edff62619c624aee223a32fd14096d24a156edbfcd3e468435bbebe22a4c2"
    }


def create_user(self, user_data):
    response = requests.post(self.base_url + self.users_endpoint, json=user_data, headers=self.headers)
    assert response.status_code == 201
    return response

def get_user(self, user_id):
    response = requests.get(f"{self.base_url}{self.users_endpoint}/{user_id}")
    assert response.status_code == 200
    return response

