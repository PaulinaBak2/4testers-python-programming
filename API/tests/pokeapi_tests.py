import requests
from utils.pokeapi_handler import PokeAPIHandler

pokeapi_handler = PokeAPIHandler()


def test_json_is_not_empty():
    response = requests.get('https://pokeapi.co/api/v2/pokemon')
    body = response.json()
    assert body != 0


def test_if_response_code_is_200():
    response = requests.get('https://pokeapi.co/api/v2/pokemon')
    body = response.json()
    assert response.status_code == 200


def test_pokemon_numbers_is_1279():
    response = requests.get('https://pokeapi.co/api/v2/pokemon')
    body = response.json()
    assert body["count"] == 1279


def test_response_time_is_under_1s():
    response = requests.get('https://pokeapi.co/api/v2/pokemon')
    body = response.json()


def test_size_of_response_is_under_100_kb():
    response = requests.get('https://pokeapi.co/api/v2/pokemon')
    response_body = response.json()
    assert len(response_body["results"]) > 0

    assert response.status_code == 200

    assert response_body["count"] == 1279

    response_time_ms = response.elapsed.microseconds // 1000
    assert response_time_ms < 1000

    response_size_bytes = len(response.content)
    assert response_size_bytes < 100 * 1000


def test_pagination():
    params = {
        "limit": 10,
        "offset": 20
    }

    response = pokeapi_handler.get_list_of_pokemons(params)
    body = response.json()

    response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
    body = response.json()

    assert len(body["results"]) == params["limit"]

    splitted = (body["results"][0]['url']).split("/")
    assert int(splitted[-2]) == (params["limit"])
    assert params["offset"] + 1 in body["results"][0]["url"]

    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + 1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset'] + params['limit']}/"


def test_pokemon_shape():
    response = pokeapi_handler.get_list_of_pokemons()
    response_body = response.json()
    assert response_body["count"] == len(response_body["results"])


def test_shapes():
    body = pokeapi_handler.get_shapes_of_pokemons().json()

    assert body["count"] == len(body["results"])

    pokemon_name = body["results"][2]["name"]
    body = pokeapi_handler.get_shapes_of_pokemons(pokemon_shape).json()
